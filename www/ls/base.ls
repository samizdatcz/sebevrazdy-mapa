dataAssoc = {}
data = d3.tsv.parse ig.data.data, (row) ->
  row.rate = parseFloat row.rate.replace ',' '.'
  dataAssoc[row.nuts] = row
topo = ig.data.topo

geojson = topojson.feature topo, topo.objects."data"
for feature in geojson.features
  feature.properties.centroid = L.latLng do
    d3.geo.centroid feature .reverse!
container = d3.select ig.containers['base']

mapContainer = container.append \div
  ..attr \class \map-container
map = mapContainer.append \div
  ..attr \class \map

map = L.map do
  * map.node!
  * maxZoom: 7
    minZoom: 5
    zoom: 5
    center: [51.5,9]

baseLayer = L.tileLayer do
  * "https://samizdat.cz/tiles/ton_b1/{z}/{x}/{y}.png"
  * zIndex: 1
    opacity: 1
    attribution: 'mapová data &copy; přispěvatelé <a target="_blank" href="http://osm.org">OpenStreetMap</a>, obrazový podkres <a target="_blank" href="http://stamen.com">Stamen</a>, <a target="_blank" href="https://samizdat.cz">Samizdat</a>'


labelLayer = L.tileLayer do
  * "https://samizdat.cz/tiles/ton_l2/{z}/{x}/{y}.png"
  * zIndex: 3
    opacity: 0.75

baseLayer.addTo map
# labelLayer.addTo map
map.on \zoomend ->
  if map.getZoom! > 6
    labelLayer.addTo map
  else
    map.removeLayer labelLayer
scale = d3.scale.quantize!
  ..domain [0 33.2]
  ..range ['#fff5f0','#fee0d2','#fcbba1','#fc9272','#fb6a4a','#ef3b2c','#cb181d','#a50f15','#67000d']
dataLayer = L.geoJson do
  * geojson
  * style: (feature) ->
      datum = dataAssoc[feature.properties.KODNUTS]
      weight = 1
      fillOpacity = 0.7
      color = if datum then scale datum.rate else \#aaa
      {color, weight, fillOpacity}
    onEachFeature: (feature, layer) ->
      datum = dataAssoc[feature.properties.KODNUTS]
      msg = "Pro toto území nejsou data k dizpocici."
      if datum
        msg = "<b>#{datum.name}</b><br>
        <b>#{ig.utils.formatNumber datum.rate, 2}</b> sebevražd na 100 000 obyvatel"
      # msg += "<br>#{feature.properties.KODNUTS}"
      layer.bindPopup msg
      layer.on \mouseover ->
        layer.openPopup feature.properties.centroid
        layer.setStyle {weight: 4}
      layer.on \mouseout ->
        layer.setStyle {weight: 1}

dataLayer.addTo map

